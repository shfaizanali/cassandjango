from rest_framework import serializers

from cassendjango_admin.models import ExampleModel


class ExampleSerializer(serializers.ModelSerializer):

    fields = '__all__'

    class Meta:
        model = ExampleModel