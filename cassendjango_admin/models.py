from django.db import models

# Create your models here.

import uuid
from cassandra.cqlengine import columns
from django_cassandra_engine.models import DjangoCassandraModel

class ExampleModel(DjangoCassandraModel):
    read_repair_chance = 0.05 # optional - defaults to 0.1
    example_id = columns.UUID(primary_key=True, default=uuid.uuid4)
    description = columns.Text(required=False)


class News(DjangoCassandraModel):

    id = columns.UUID(primary_key=True, default=uuid.uuid4)
    user = columns.Integer(index=True)
    page = columns.Integer()
    news_ids = columns.List(value_type=columns.Integer)