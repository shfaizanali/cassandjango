from django.apps import AppConfig


class CassendjangoAdminConfig(AppConfig):
    name = 'cassendjango_admin'
