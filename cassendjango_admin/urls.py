from django.urls import path

from cassendjango_admin.views import TestView, FetchNewsView

app_name = 'cassendjango_admin'

urlpatterns = [
    path('test/', TestView.as_view(), name='test_view'),
    path('retrieve/feed/<int:user>/<int:page>/', FetchNewsView  .as_view(), name='test_view'),
]