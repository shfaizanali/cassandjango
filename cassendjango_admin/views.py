import json

from cassandra.cluster import Cluster
from django.http import HttpResponse
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from cassendjango_admin.models import News
from django.core.cache import cache
cluster = Cluster(['127.0.0.1'])
session = cluster.connect()
session.set_keyspace('cassendjango_db')

def index(request):
    return HttpResponse("Hello world")


class TestView(APIView):

    def get(self, request, **kwargs):
        id = [102, 202, 302, 402, 502, 602, 702, 802, 902, 1202,
              11,21,31,41,51,61,71,81,91,112,
              1,2,3,4,5,6,7,8,9,10]
        for i in id:
            json = {
                "id": i,
                "title": "جمائمہ کے بھائی کی فلسطینیوں کے زخم پر نمک پاشی",
                "description": "لندن کی میئر شپ کے حصول میں ناکام ہونے اورموجودہ میئر صادق خان کے ہاتھوں بری شکست سے دوچارہونے والے ",
                "link": "https://jang.com.pk/news/494033",
                "is_featured": True,
                "is_liked": False,
                "pub_date": "2018-05-17T10:31:34Z",
                "comments_count": 2,
                "like_count": 4,
                "image": "https://jang.com.pk/assets/uploads/updates/2018-05-17/494033_3754571_updates.jpg",
                "source": 14,
                "category": 2,
                "lang": 2,}
            cache.set(i, json, 60*60*60)
        query = session.prepare("""
            INSERT INTO news (id,user, page, news_ids)
            VALUES (now(),1, 2, [102, 202, 302, 402, 502, 602, 702, 802, 902, 1202])
            """)
        session.execute(query)
        # insert1 = News(page=1, user=1, news_ids=[1,2,3,4,5,6,7,8,9,11], keyspace='cassendjango_db')
        # insert2 = News(page=3, user=1, news_ids=[102, 202, 302, 402, 502, 602, 702, 802, 902, 1202], keyspace='cassendjango_db')
        # # insert.save()
        # insert1.save()
        # insert2.save()
        # cluster.shtdown()
        return Response({'results':'Cache has been updated', 'status': status.HTTP_200_OK})


class FetchNewsView(APIView):

    def get(self, request, **kwargs):
        page = kwargs.get('page')
        user = kwargs.get('user')
        query = "select * from cassendjango_db.news where user=1 and page =2 allow filtering"
        query_session = session.prepare(query=query)
        insert = session.execute(query_session)
        # user_details = News.objects.filter(user=user, page=page)
        # # insert = News()
        response = []
        # insert=user_details.get()
        # #
        for id in insert[0].news_ids:
            response.append(cache.get(id))

        return Response({'results':response, 'status': status.HTTP_200_OK})
